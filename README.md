# JRJC Usage Example

This is just an example maven project that uses JRJC.

You'll have to add atlassian public maven repository (https://maven.atlassian.com/repository/public) to your maven settings.xml before building that project.

To build project run: 
$ mvn package

To build distribution packages run:
$ mvn assembly:assembly

To run project execute under target directory:
$ java -jar jrjc-example-1.0-SNAPSHOT.jar 
